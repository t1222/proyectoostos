
# Se importan las librerías
from multiprocessing.pool import RUN
import time
from turtle import Screen
import pygame
import numpy as np

#Color para el fondo, oscuro
COLOR_BG = (10, 10, 10)

#Color para el grid
COLOR_GRID = (40, 40 ,40)

#Color para cuando una celula vaya a morir
COLOR_DIE_NEXT =(170, 170 ,170)

#Color para cuando una celula este viva
COLOR_ALIVE_NEXT =(255, 255 ,255)

#Función principal, cells es el estado de las celulas individuales, size es el tamaño de la celula
# with_progress es para cuando queramos actualizar la pantalla sin movernos a la siguiente generación
def update(screen, cells , size , with_progress=False):
    update_cells = np.zeros((cells.shape[0], cells.shape[1]))
    #Crea un array vacio, en este array se aplicaran los cambios

    #Pasa por todas las celulas y aplica las reglas del juego a cada una
    for row, col in np.ndindex(cells.shape):
        #Sumas las celulas pegadas y restas la misma celula
        alive = np.sum(cells[row-1:row+2, col-1:col+2]) - cells[row,col]
        
        #Por default el color es el color del fondo, si no entonces el color es celula viva 
        color = COLOR_BG if cells[row,col] == 0 else COLOR_ALIVE_NEXT

        #Comprobamos la celula viva, si esta tiene 2 o 3 celulas vivas
        #esta muere en la siguiente iteración, si progreso esta activo
        if cells[row,col]==1:
            if alive < 2 or alive > 3:
                if with_progress:
                    color = COLOR_DIE_NEXT
            elif 2 <= alive <= 3:  #Si la celula tiene 2 o 3 celulas vivas, en la siguiente iteración vivira
                update_cells[row, col] = 1
                if with_progress:
                    color = COLOR_ALIVE_NEXT
        else:
            if alive == 3: #Si la celula tiene 3 vecinos, esta celula nacera
                update_cells[row,col]=1
                if with_progress:
                    color= COLOR_ALIVE_NEXT
    #Se dibuja la pantalla
        pygame.draw.rect(screen, color,(col * size, row * size, size -1, size-1))
    
    #Se regresa la actualización del juego
    return update_cells

def main():
    pygame.init() #Se inicializa pygame
    screen = pygame.display.set_mode((600,600)) #Se crea la pantalla, resolución

    cells= np.zeros((60,60)) #Se crea el grid, en este caso 60 * 80
    screen.fill(COLOR_GRID) #Se llena la pantalla con el color del grid
    update(screen, cells, 10) #Se llena la pantalla con el color del grid

    pygame.display.flip()
    pygame.display.update()

    running = False

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            elif event.type == pygame.KEYDOWN: #Si presionamos una tecla
                if event.key == pygame.K_SPACE:
                    running = not running
                    update(screen, cells ,10)
                    pygame.display.update()
                if event.key == pygame.K_1:
                    running = not running
                    cells= np.random.randint(2, size=(60,60))
                    update(screen, cells, 10, False)
                    pygame.display.update()
                if event.key == pygame.K_2:
                    running = not running
                    cells= np.zeros((60,60))
                    update(screen, cells ,10)
                    pygame.display.update()
            if pygame.mouse.get_pressed()[0]:
                pos = pygame.mouse.get_pos()
                cells[pos [1] // 10 , pos[0] // 10 ]= 1
                update(screen, cells , 10)
                pygame.display.update()

        screen.fill(COLOR_GRID)

        if running:
            cells = update (screen, cells , 10 , with_progress=True)
            pygame.display.update()

        time.sleep(0.001)

if __name__== '__main__':
    main()