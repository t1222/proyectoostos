def get_adjacent_values(arr, row, col):
    adjacent_values = []
    num_rows = len(arr)
    num_cols = len(arr[0])
    
    # check top adjacent value
    if row > 0:
        adjacent_values.append(arr[row-1][col])
    
    # check bottom adjacent value
    if row < num_rows-1:
        adjacent_values.append(arr[row+1][col])
    
    # check left adjacent value
    if col > 0:
        adjacent_values.append(arr[row][col-1])
    
    # check right adjacent value
    if col < num_cols-1:
        adjacent_values.append(arr[row][col+1])
    
    return adjacent_values

def main():

    while True:
        # create a 3x3 array
        my_array = [[1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 0]]

        # print the array
        for row in my_array:
            print(row)

        # get user input
        selection = input("Escribe el valor que deseas mover o presiona enter para salir: ")
        if not selection:
            break

        # find the coordinates of the selected value
        for i in range(3):
            for j in range(3):
                if my_array[i][j] == int(selection):
                    selected_row = i
                    selected_col = j
                    adjacent_values = get_adjacent_values(my_array, selected_row, selected_col)
                    if 0 in adjacent_values:
                        # find the coordinates of the empty space
                        for i1 in range(3):
                            for j1 in range(3):
                                if my_array[i1][j1] == 0:
                                    empty_row = i1
                                    empty_col = j1

                        # swap the selected value and the empty space
                        my_array[selected_row][selected_col] = 0
                        my_array[empty_row][empty_col] = int(selection)

                        
                    else:
                        print("El numero que escogiste no es adjacente al espacio vacio.")

        # print the updated array
            for row in my_array:
                print(row)
            print("\n")
    
if __name__== '__main__':
    main()
