#Mi intento de crear el puzzle
#Ruy Jesé Luna Sandoval

def get_adjacent_values(arr, row, col):
    adjacent_values = []
    num_rows = len(arr)
    num_cols = len(arr)
    
    # check top adjacent value
    if row > 0:
        adjacent_values.append(arr[row-1][col])
    
    # check bottom adjacent value
    if row < num_rows-1:
        adjacent_values.append(arr[row+1][col])
    
    # check left adjacent value
    if col > 0:
        adjacent_values.append(arr[row][col-1])
    
    # check right adjacent value
    if col < num_cols-1:
        adjacent_values.append(arr[row][col+1])
    
    return adjacent_values

def main():
    # create a 3x3 array
    my_array = [[1, 2, 3],
                [4, 5, 6],
                [7, 8, 0]]

    # print the array
    for row in my_array:
        print(row)

    # get user input
    selection = input("Enter the value you want to move: ")

    # find the coordinates of the selected value
    for i in range(3):
        for j in range(3):
            if my_array[i][j] == int(selection):
                selected_row = i
                selected_col = j
                adjacent_values = get_adjacent_values(my_array, selected_row, selected_col)
                if 0 in adjacent_values:
                    # find the coordinates of the empty space
                    for i in range(3):
                        for j in range(3):
                            if my_array[i][j] == 0:
                                empty_row = i
                                empty_col = j

                    # swap the selected value and the empty space
                    my_array[selected_row][selected_col] = 0
                    my_array[empty_row][empty_col] = int(selection)

                    # print the updated array
                    for row in my_array:
                        print(row)
                else:
                    print("El numero que escogiste no es adjacente al espacio vacio.")

    
if __name__== '__main__':
    main()
